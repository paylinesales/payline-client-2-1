import React, { ReactElement } from "react";
import Image, { ImageProps } from "next/image";
import Link, { LinkProps } from "next/link";
import { ChevronRightIcon } from "@heroicons/react/outline";

type PostProps = {
  postTitle: string | null;
  excerpt: string | null;
  date: string | null;
  imageUrl: string;
};

// Intersection of the three interfaces which combine to form one type.

type BlogProps = PostProps & ImageProps & LinkProps;

function BlogCard({
  imageUrl,
  postTitle,
  href = "",
  excerpt,
  date,
}: BlogProps): ReactElement {
  return (
    <div className="flex flex-col min-w-full	swiper-slide rounded-lg shadow-lg overflow-hidden h-96 xl:max-w-md">
      <div className="flex-shrink-0">
        {imageUrl && (
          <Image
            id="swiper-slide__image"
            className=""
            src={imageUrl}
            height="250"
            width="400"
          />
        )}
      </div>
      <div className="flex-1 bg-white p-6 flex flex-col justify-center">
        <div className="flex-1 text-left">
          <p className="text-sm font-medium text-indigo-600">
            <Link href={href}>
              <a className="hover:underline">{date}</a>
            </Link>
          </p>
          <Link href={href}>
            <a className="block mt-2">
              <p className="text-xl font-semibold text-gray-900">{postTitle}</p>
              <p className="mt-3 text-base text-gray-500">{excerpt}</p>
            </a>
          </Link>
        </div>
        <div className="mt-6items-center">
          <Link href={href}>
            <a className="text-lg	 font-opensans font-bold text-payline-black  flex flex-row align-center ">
              Read Now <ChevronRightIcon className="ml-5 h-6 w-6" />
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
}
// change branch

export default BlogCard;
