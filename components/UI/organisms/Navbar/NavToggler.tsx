import React from "react";
import Image from "next/image";

const NavToggler: React.FC<{ toggleMobileMenuHandler: () => void }> = (
  props,
) => {
  const { toggleMobileMenuHandler } = props;
  return (
    <div className="xl:hidden flex items-center">
      <button
        type="button"
        className="outline-none mobile-menu-button"
        onClick={toggleMobileMenuHandler}>
        <Image src="/images/svg/menu-lines.svg" width={21} height={22} />
      </button>
    </div>
  );
};
export default NavToggler;
