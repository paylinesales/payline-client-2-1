import React, { ReactElement } from "react";

function TrapezoidIcon(): ReactElement {
  return (
    <div>
      <svg
        width="45"
        height="43"
        viewBox="0 0 45 43"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          d="M38.7711 1.32021L37.985 0.526123L0.771118 37.732L1.55724 38.5261L3.80831 36.2755L26.7663 42.4271L44.1185 25.0749L37.9684 2.12271L38.7711 1.32021Z"
          fill="#7FA9B3"
        />
      </svg>
    </div>
  );
}

export default TrapezoidIcon;
