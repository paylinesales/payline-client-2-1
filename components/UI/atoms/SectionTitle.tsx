/* eslint-disable react/require-default-props */
import React from "react";
import { Scalars } from "@/generated/apolloComponents";
import HalfCircle from "./Icons/HalfCircle";
import ColorTextBox from "./ColorTextBox";

interface SectionTitleProps {
  firstPartOfHeadline?: Scalars["String"];
  subtitle?: Scalars["String"];
  highlighedText: Scalars["String"];
  lastPartOfHeadline?: Scalars["String"];
}

const SectionTitle = (props: SectionTitleProps): React.ReactElement => {
  const { subtitle, firstPartOfHeadline, highlighedText, lastPartOfHeadline } =
    props;
  return (
    <>
      {subtitle !== undefined && (
        <p className="uppercase text-center">{subtitle}</p>
      )}
      <div className="flex items-center gap-3 mt-6">
        <HalfCircle className="hidden lg:block" />
        <h2 className="text-2xl md:text-4xl leading-tight font-bold text-payline-black mx-auto text-center">
          {firstPartOfHeadline || ""}
          <ColorTextBox>{highlighedText}</ColorTextBox>
          {lastPartOfHeadline || ""}
        </h2>
      </div>
    </>
  );
};

SectionTitle.defaultProps = {
  subtitle: undefined,
};

export default SectionTitle;
