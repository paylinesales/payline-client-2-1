import React from "react";

interface IPaylineButtonProps {
  className?: string;
  children?: React.ReactNode;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  disabled?: boolean;
  value?: number | string;
}

const PaylineButton: React.FC<IPaylineButtonProps> = (props) => {
  const { children, className = "", onClick, disabled = false, value } = props;
  return (
    <button
      disabled={disabled}
      type="button"
      onClick={onClick}
      className={`${className} border border-solid border-payline-border-light rounded p-3`}
      data-value={value}>
      {children}
    </button>
  );
};

PaylineButton.defaultProps = {
  className: "",
  children: null,
  onClick() {
    // eslint-disable-next-line no-console
    console.log("Clicked");
  },
  disabled: false,
  value: "",
};

export default PaylineButton;
