/* eslint-disable react/require-default-props */
import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleUp, IconDefinition } from "@fortawesome/free-solid-svg-icons";
import { ICON_POSITION } from "@/constants/*";

const { LEFT, RIGHT } = ICON_POSITION;

type detailProps = {
  className?: string;
  title: string;
  summaryClasses?: string;
  iconPosition?: ICON_POSITION;
  icon?: IconDefinition;
  iconWhenOpened?: IconDefinition;
};

const PaylineDetail: React.FC<detailProps> = (props) => {
  const {
    title,
    children,
    className: detailsClasses = "",
    summaryClasses: summaryPropClasses = "",
    iconPosition = LEFT,
    icon = faAngleUp,
    iconWhenOpened = icon,
  } = props;
  const iconPositionStyle = iconPosition === RIGHT ? "justify-between" : "";
  const summaryClasses = `py-5 px-8 cursor-pointer flex items-center ${iconPositionStyle} ${summaryPropClasses}`;
  const [isOpened, setIsOpened] = useState(false);
  const updateIcon = () => {
    setIsOpened((prevState) => {
      return !prevState;
    });
  };
  const getIcon = () => {
    if (!isOpened) {
      return icon;
    }
    return iconWhenOpened;
  };
  return (
    <details className={`payline-details mb-5 ${detailsClasses}`}>
      <summary className={summaryClasses} onClick={updateIcon}>
        {iconPosition === LEFT && (
          <FontAwesomeIcon
            icon={getIcon()}
            className="payline-details__icon mr-7"
          />
        )}
        {title}
        {iconPosition === RIGHT && (
          <FontAwesomeIcon icon={getIcon()} className="payline-details__icon" />
        )}
      </summary>
      <div className="px-12 text-payline-dark pb-5">{children}</div>
    </details>
  );
};

export default PaylineDetail;
