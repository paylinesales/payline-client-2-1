import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import { useQuery } from "@apollo/client";
import WhiteButton from "./WhiteButton";
import grabPostQuery from "../../../queries/grab-posts";

export type BlogPostType = {
  id: string; // post.id
  slug: string; // post.slug
  title: string; // post.title
  categories: [name: string]; // post.categories.nodes
  description: string; // post.excerpt
  datetime: string; // post.date
  imageUrl: string; // post.featuredImage
  author: {
    name: string; // `${post.author.node.firstname} ${post.node.lastname}`
    href: string; // post.author.node.url
    imageUrl: string; // post.author.node.avatar.url
  };
};

const BlogPost = ({ post, setCategories, categories }) => {
  const { loading, error, data } = useQuery(grabPostQuery, {
    variables: {
      id: post,
    },
  });

  if (loading) {
    return <div />;
  }

  if (error) {
    return (
      <div className="py-12 md:px-8 md:bg-blog-search-oval md:bg-cover md:bg-no-repeat">
        <b>
          <h1 className="px-4 text-4xl text-payline-black">Error {error}</h1>
        </b>
      </div>
    );
  }

  if (data) {
    const postData = {
      id: post,
      slug: data.post.slug,
      title: data.post.title,
      categories: data.post.categories.nodes,
      description: data.post.excerpt,
      datetime: data.post.date,
      imageUrl: data.post.featuredImage
        ? data.post.featuredImage.node.sourceUrl
        : data.post.featuredImage,
      author: {
        name: `${data.post.author.node.firstName} ${data.post.author.node.lastName}`,
        href: data.post.author.node.url,
        imageUrl: data.post.author.node.avatar.url,
      },
    };
    return (
      <div key={postData.id}>
        <div className="flex my-5">
          <div className="w-full md:w-7/10">
            <div className="px-4 flex gap-2">
              <Image src="/images/svg/ach.png" width={16} height={16} />
              <p className="text-orange">
                {moment(postData.datetime).format("DD, MMMM, YYYY")}
              </p>
            </div>

            <div className="md:hidden">
              <div className="px-2 flex items-center gap-3 p-3">
                <div className="mx-3">
                  <Link
                    href={
                      postData.author.href != null ? postData.author.href : "#"
                    }>
                    <img
                      className="h-10 w-10 min-w-min min-h-min rounded-full"
                      src={postData.author.imageUrl}
                      alt=""
                    />
                  </Link>
                </div>
                <div className="font-bold">
                  <span>{postData.author.name}</span>
                </div>
              </div>
            </div>

            <h1 className="px-4 text-4xl text-payline-black">
              <b>{postData.title}</b>
            </h1>
            <div
              className="px-4 my-6"
              dangerouslySetInnerHTML={{ __html: postData.description }}
            />
            {/* 
                since gaining access of the wordpress database would be the only way to exploit the dangerouslySetInnerHTML function,
                this is mostly safe -- the website would be compromised if the database was compromised anyway, so this doesn't increase
                risk
              */}
            <div className="flex items-center">
              <div className="flex flex-wrap gap-3 px-4 my-6">
                {postData.categories.map((c) => {
                  return (
                    <WhiteButton
                      className="rounded-full"
                      key={c.id}
                      selected={categories.indexOf(c.name) > -1}
                      onClick={() => {
                        if (categories.indexOf("") > -1) {
                          setCategories([c.name]);
                        } else if (categories.indexOf(c.name) > -1) {
                          setCategories(
                            categories.filter(
                              (category) => category !== c.name,
                            ),
                          );
                        } else {
                          setCategories((oldCategories) => [
                            ...oldCategories,
                            c.name,
                          ]);
                        }
                        document.getElementById("categoryHolder")?.focus();
                      }}>
                      {c.name}
                    </WhiteButton>
                  );
                })}
              </div>
              <div className="flex gap-3 px-4 my-6">
                <div className="text-payline-black font-bold text-lg">
                  <Link href={`/blog/${postData.slug}`}>Read</Link>{" "}
                </div>
                <Image
                  src="/images/svg/chevron-right-black.svg"
                  width={8}
                  height={15}
                />
              </div>
            </div>
          </div>
          <div className="hidden md:block pt-4">
            <div className="flex items-center gap-3 p-3">
              <div className="mx-3">
                <Link
                  href={
                    postData.author.href != null ? postData.author.href : "#"
                  }>
                  <img
                    className="h-10 w-10 min-w-min min-h-min rounded-full"
                    src={postData.author.imageUrl}
                    alt=""
                  />
                </Link>
              </div>
              <div>
                <b>{postData.author.name}</b>
              </div>
            </div>
            <img src={postData.imageUrl} className="w-64 p-3" />
          </div>
        </div>
      </div>
    );
  }
  return <div />;
};

export default BlogPost;
