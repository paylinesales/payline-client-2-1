import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";

type linkProps = {
  className?: string;
  icon: IconDefinition;
  href: string;
};

const FooterIcon: React.FC<linkProps> = (props) => {
  const { className, icon, href: url } = props;
  const classes = className === undefined ? "" : className;
  return (
    <a
      href={url}
      target="_blank"
      rel="noreferrer"
      className={`rounded-full bg-payline-dark bg-opacity-10 flex items-center justify-center p-3 text-payline-dark cursor-pointer hover:bg-payline-black hover:bg-opacity-10 transition-colors duration-500 ${classes}`}>
      <FontAwesomeIcon icon={icon} />
    </a>
  );
};

FooterIcon.defaultProps = {
  className: undefined,
};

export default FooterIcon;
