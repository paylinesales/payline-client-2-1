import React from "react";
import ColorTextBox from "@/components/UI/atoms/ColorTextBox";

const QuestionsAboutSolutions: React.FC = () => {
  return (
    <div className="w-full lg:w-6/12 lg:px-12">
      <h3 className="text-lg font-semibold text-payline-black mb-6 text-center">
        <span>I've got</span>
        <ColorTextBox>questions</ColorTextBox>
        <span>about</span>
        <ColorTextBox>Small Business solutions</ColorTextBox>
      </h3>
      <img
        src="/images/svg/in-touch-top-arrow.svg"
        className="w-6/12 lg:w-4/12 mx-auto"
        width={190}
        height={88}
      />
      <div className="w-6/12 lg:w-5/12 mx-auto">
        <img
          src="/images/svg/sales-team.svg"
          className="w-full"
          width={190}
          height={88}
        />
        <div className="font-bold text-2xl text-payline-black text-center mt-2">
          Sales Team
        </div>
        <div className="text-center text-payline-dark mb-2">Online now</div>
        <img
          src="/images/svg/in-touch-bottom-arrow.svg"
          className="w-full"
          width={190}
          height={88}
        />
      </div>
      <div className="w-full flex flex-wrap items-center justify-center  gap-8 lg:gap-0 mt-2 px-6 lg:px-0">
        <button
          type="button"
          className="w-full md:w-6/12 border border-solid border-payline-border-light rounded-full bg-payline-black text-white px-5 py-2">
          Request a Callback
        </button>
        <div className="w-full lg:w-6/12 text-center">
          <p>Call now</p>
          <p className="font-bold text-payline-black">(800)-887-3877</p>
        </div>
      </div>
    </div>
  );
};

export default QuestionsAboutSolutions;
