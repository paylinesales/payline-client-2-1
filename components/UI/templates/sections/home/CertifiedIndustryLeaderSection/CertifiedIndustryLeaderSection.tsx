import React from "react";
import Image from "next/image";
// import ColorTextBox from "@/components/UI/atoms/ColorTextBox";
// import WhiteButton from "../../../atoms/WhiteButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";
import SectionTitle from "@/components/UI/atoms/SectionTitle";
import Tags from "./Tags";

const tags = [
  "All",
  "Medical",
  "Omni Channel",
  "Hub and Spoke",
  "Franchises",
  "High Risk",
  "Ecommerce",
];

const CertifiedIndustryLeaderSection: React.FC = () => {
  return (
    <section
      id="certified-industry-leader-section"
      className="container mx-auto  my-0 lg:my-12 py-10 px-8 md:px-16 lg:px-20 xl:px-9">
      <SectionTitle
        firstPartOfHeadline="Certified Industry"
        highlighedText="Leader"
      />
      <div
        className="mt-16 my-16 lg:w-9/12 mx-auto flex flex-wrap items-center justify-center flex flex-row flex-wrap
        gap-x-6 gap-y-8">
        <Image
          src="/images/occasion.png"
          width={110}
          height={32}
          className="object-contain"
        />
        <Image
          src="/images/ace.png"
          width={110}
          height={32}
          className="object-contain"
        />
        <Image
          src="/images/school.png"
          width={110}
          height={32}
          className="object-contain"
        />
        <Image
          src="/images/forever.png"
          width={110}
          height={32}
          className="object-contain"
        />
        <Image
          src="/images/boonli.png"
          width={110}
          height={32}
          className="object-contain"
        />
        <Image
          src="/images/payzer.png"
          width={110}
          height={32}
          className="object-contain"
        />
      </div>
      <Tags tags={tags} className="lg:w-11/12 mx-auto" />
      <div className="w-fit-content mx-auto text-xl font-bold mt-12">
        <a href="#">
          Read
          <FontAwesomeIcon icon={faAngleRight} className="ml-8" />
        </a>
      </div>
    </section>
  );
};

export default CertifiedIndustryLeaderSection;
