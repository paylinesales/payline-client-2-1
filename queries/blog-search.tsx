import { gql } from "@apollo/client";

const blogSearchQueries = gql`
  query blogSearchQueries($categories: [String]!, $search: String!) {
    categories(first: 10000, where: { name: $categories }) {
      nodes {
        name
        id
        posts(first: 10, where: { search: $search }) {
          pageInfo {
            endCursor
          }
          nodes {
            id
          }
        }
      }
    }
  }
`;

export default blogSearchQueries;
