import { ApolloClient, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
  uri: "https://paylinelocal.wpengine.com/graphql",
  cache: new InMemoryCache(),
});

export default client;
